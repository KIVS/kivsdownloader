/*
 * ftpGet.h
 *
 *  Created on: 26.11.2013
 *      Author: Marc
 */

#ifndef FTPGET_H_
#define FTPGET_H_

#include <stdio.h>
#include <unistd.h>
#include <string.h>
//link to libcurl!
#include <curl/curl.h>

int download(const char *url, const char *fname);

#endif /* FTPGET_H_ */
