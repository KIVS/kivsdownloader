#include "ftpGet.h"

/**
* @brief downloads the file at url to local file fname
*
* @param url url to the file to load
* @param fname local filename to save to
*
* @return errorcode
*/
int download(const char *url, const char *fname)
{
	CURLcode res;

    CURL *curl = curl_easy_init();
    if(!curl) return 1;

	FILE* file=fopen(fname, "w+, ccs=UTF-8");
	if(!file) return 2;

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)file);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);
#ifndef NDEBUG
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#endif

	res = curl_easy_perform(curl);

	curl_easy_cleanup(curl);

	fclose(file);

	if(CURLE_OK != res)
		fprintf(stderr, "curl error %d\n", res);

	return 0;
}
